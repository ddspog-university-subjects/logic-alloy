# Alloy-Tema8
Projeto de Alloy para disciplina Matemática Lógica da UFCG
Tema 8:
Como cientistas da computação um grupo de alunos que tiraram nota boa em Lógica Matemática na UFCG foi convidado a criar o programa de um sistema muito complexo, esse grupo de alunos da UFCG foi dividida em 2 equipes de desenvolvedores, uma terceira equipe de uma empresa terceirizada foi contratada só para testar esse programa, essas duas equipes estão sempre trabalhando em partes diferentes do programa e a equipe de teste está testando sempre uma das partes que acabou o desenvolvimento. No começo a equipe de testes está desalocada de tarefas e o projeto acaba quando a equipe de testes testa todo o projeto. Cada módulo pode estar em vários estados diferentes : em desenvolvimento, em testes, integrado ou entregue.
Integrantes: Dennis Dantas;
             Gustavo Monteiro (@GustavoMA); 
             Mateus Dantas;
             Árysson Cavalcanti;
