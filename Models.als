one sig Program {
	modules : set Module
}

sig Module {
}

sig DevelopingModule extends Module {
}

sig TestingModule extends Module {
}

sig IntegratingModule extends Module {
}

sig DeliveredModule extends Module {
}

sig Team {
	
} {
	#Team = 3
}

sig DevelopmentTeam extends Team {

} {
	#DevelopmentTeam = 2
}

one sig TestingTeam extends Team {

}

//buged
pred show[] {}
run show for 2
